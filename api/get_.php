<?php
//CONNECT TO DATABASE
//LINK DO HOST, SE FOR LOCAL É LOCALHOST|USUARIO|SENHA|NOME DO BANCO
$con = mysqli_connect('localhost','root','root','hack_db');
$con->set_charset("utf8");

//GET THE FUNCTION TO DO IN THIS SERVE CALL
$func = htmlspecialchars($_GET['function']);

if(empty($func)) echo '{"response":"empty_variable"}';
else{
    //CHECK IF CONNECTION GET ANY ERROR
    if ($con->connect_error) die('{"response":"'.$con->connect_error.'"}');
    else{
        //IF CONNECTION WORK ANYWAY - REDUNDANT
        if($con){
            $result = mysqli_query($con, "SELECT * FROM `hack_table`");
            show($result);
        }else echo '{"response":"not_database_connection"}';
    }
}
//CREATE THE JSON LAYOUT BY THE $result VARIABLE
function show($result){
    if($result->num_rows != 0) {
        //JSON CONSTRUCTOR
        echo '{"list":{';
            while($rowData = mysqli_fetch_array($result)) {
                echo '"coluna_1": "'.$rowData["coluna_1"].'", 
                    "coluna_2": "'.$rowData["coluna_2"].'",';
            }
            echo '"":""},"response": "success"}';
            //END JSON AND RESPONSE RESULT SUCCESS
    }else echo '{"response": "empty_db"}';
}
?>