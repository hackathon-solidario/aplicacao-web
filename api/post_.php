<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//SET TIME ZONE FOR UTC
date_default_timezone_set('America/Bahia');

//Getting json code and converto to php data vector
$_POST = json_decode(file_get_contents('php://input'), true);

//GET $_POST VARIABLES
$dado_1 = htmlspecialchars($_POST['dado_1']);
$dado_2 = htmlspecialchars($_POST['dado_2']);
//GET DATE AND TIME OF THIS REQUEST
$date = date('d m Y');
$hour = date('H:i:s');

//CONNECT TO DATABASE
//LINK DO HOST, SE FOR LOCAL É LOCALHOST|USUARIO|SENHA|NOME DO BANCO
$con = mysqli_connect('localhost','root','root','hack_db');
$con->set_charset("utf8");

if ($con->connect_error) die('{"response":"'.$con->connect_error.'"}');

else{
    //CHECKING IF SOME VARIABLES ARE EMPTY OR IF HAVE DATABASE CONNECTION
    if(empty($dado_1) || empty($dado_2) echo '{"response":"empty_variable"}';

    else if($con){

        //POST ON DATABASE
        $sql = mysqli_query($con, "INSERT INTO `hack_table` (`coluna_1`,`coluna_2`) 
        VALUES ('$dado_1','$dado_2')");

        if ($con->query($sql) === TRUE) {
            echo '{"response": "success"}';
        } else {
            echo '{"response": "'.$sql.$con->error.'"}';
        }
    }else{
        echo '{"response":"not_database_connection"}';
    }
}
?>